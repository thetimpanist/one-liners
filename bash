# remove already merged branches from git repo
git branch --merged | egrep -v "(^\*|master|dev)" | xargs git branch -d

# get recent amazon ami
aws ec2 describe-images --owners amazon --filters "Name=root-device-type,Values=ebs" "Name=name,Values=amzn2-ami-minimal-hvm*" --query "Images[*].[CreationDate, Description]"

# realtime mysql monitor
mysql -u<username> -Ap<password>  -h<endpoint> -e 'show full processlist;'
